package de.ppi.oss.zulipconst;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class EmojiTest {

    @Test
    public void getUnicode() {
        assertEquals("\uD83D\uDC40", Emoji.EYES.getUnicode());
    }

    @Test
    public void getNameInZulip() {
        assertEquals("eyes", Emoji.EYES.getNameInZulip());
    }
}
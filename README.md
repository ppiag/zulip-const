# zulip-const

All constants migrated from python to java. 
Need for kzulip, create because a kotlin-bug [KT-31100](https://youtrack.jetbrains.com/issue/KT-31100)
If you want to use it add the following dependency to your pom.

    <dependency>
        <groupId>de.ppi.oss</groupId>
        <artifactId>zulip-const</artifactId>
        <version>0.1.0</version>
    </dependency>

